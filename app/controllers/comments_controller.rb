class CommentsController < ApplicationController
  
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(params[:comment].permit(:name, :body))
 
    if @comment.save
      redirect_to post_path(@post)
    else
      flash[:notice] = "The name field must have a minimum of 3 characters and the body field cannot be blank!"
      redirect_to post_path(@post)
    end
    
  end

  def destroy
  	@post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy
 
    redirect_to post_path(@post)
  end
  
end