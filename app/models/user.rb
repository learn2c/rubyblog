class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, # THIS MAKES IT SO USERS CANNOT REGISTER
         :recoverable, :rememberable, :trackable, :validatable
end
